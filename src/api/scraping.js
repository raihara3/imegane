// expressモジュールを読み込む
const express = require('express')
const request = require('request')
const path = require("path")

const REQUEST_ALLOW_LIST = require('../api/request_allow_list')

// expressアプリを生成する
let app = express()
// dist内を公開する
// app.use(express.static(path.resolve("../")))

// アクセスしてきたときに
app.get('/', (req, res) => {
// app.get('/api/', (req, res) => {
  // let target_url = encodeURI(req.query.url)
  let target_url = req.query.url

  let query_key = Object.keys(req.query)
  for(let i = 0; i < query_key.length; i++) {
    let key = query_key[i]
    if(req.query[key] != null && key != 'url') {
      target_url = target_url + '&' + key + '=' + req.query[key]
    }else if(key != 'url') {
      target_url = target_url + '&' + key + '='
    }
  }
  // query_key.forEach((key) => {
  //   if(req.query[key] != null && key != 'url') {
  //     target_url = target_url + '&' + key + '=' + req.query[key]
  //   }else if(key != 'url') {
  //     target_url = target_url + '&' + key + '='
  //   }
  // })
  console.log(target_url)
  target_url = encodeURI(target_url)

  let result = REQUEST_ALLOW_LIST(target_url)
  if(!result.callback) {
    res.json({
      error: 'Do not allow access to sites other than designated sites'
    })
    return
  }

  let options = {
    url: target_url,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    json: true
  }
  request(options, (error, req, body) => {
    res.json(body)
  })
})

// ポート3000でサーバを立てる
app.listen(3000, () => console.log('Listening on port 3000'))