module.exports = (target_url) => {
  let search_flg = false

  const ALLOW_LIST = [
    'https://pixta.jp', //まてりこ --->
    'https://snapmart.jp/', //
    'https://girlydrop.com/', //
    'https://jp.fotolia.com', //
    'https://jp.123rf.com/', //
    'https://www.ac-illust.com', //
    'https://www.silhouette-ac.com/', //
    'http://www.map-ac.com/', //
    'http://icooon-mono.com/', //
    'http://flat-icon-design.com/', // <--- まてりこ

    'https://www.photo-ac.com',
    'https://pixabay.com',
    'https://freephotos.cc/ja',
    'https://www.foodiesfeed.com',
    'https://freerangestock.com',
    'https://visualhunt.com',
    'https://shotstash.com',
    'https://gratisography.com',
    'https://unsplash.com'
  ]

  target_url = target_url.replace(/\/|\:|\./g, '')
  ALLOW_LIST.forEach(value => {
    value = value.replace(/\/|\:|\./g, '')
    if(target_url.indexOf(value) == 0) {
      search_flg = true
      return
    }
  })

  return {
    callback: search_flg
  }
}