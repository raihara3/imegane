import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

import MagnifyBox from './MagnifyBox'

export default class MagnifyImg extends React.Component {

  static get propTypes() {
    return {
      download_url: PropTypes.array.isRequired,
      img_src: PropTypes.array.isRequired
    }
  }

  constructor(props) {
    super(props)
    this._on_click = this._on_click.bind(this)
  }

  _on_click() {
    ReactDOM.render (
      React.createElement (
        MagnifyBox, {download_url: this.props.download_url, img_src: this.props.img_src}
      ),
      document.getElementById('magnify-box')
    )
  }

  render() {
    return (
      React.createElement (
        'div', {className: 'magnify-btn', onClick: this._on_click},
        React.createElement (
          'i', {className: 'fas fa-search-plus'}
        )
      )
    )
  }
}