import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

export default class MagnifyBox extends React.Component {

  static get propTypes() {
    return {
      download_url: PropTypes.array.isRequired,
      img_src: PropTypes.array.isRequired
    }
  }

  constructor(props) {
    super(props)
    this._close_box = this._close_box.bind(this)
  }

  _close_box() {
    ReactDOM.unmountComponentAtNode (
      document.getElementById('magnify-box')
    )
  }

  render() {
    return (
      React.createElement (
        'div', {id: 'magify-inner', onClick: this._close_box},
          React.createElement (
          'i', {className: 'magnify-box-close-btn far fa-times-circle', onClick: this._close_box}
        ),
        React.createElement (
          'a', {href: this.props.download_url, target: "_blank"},
          React.createElement (
            'img', {src: this.props.img_src}
          ),
          React.createElement (
            'p', {className: 'annotation'}, '※ダウンロードできる画像より低画質です'
          )
        )
      )
    )
  }
}