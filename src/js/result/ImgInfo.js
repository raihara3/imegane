import React from 'react'
import PropTypes from 'prop-types'

const SEARCH_SITE_INFO = require('../SearchSiteInfo')

export default class ImgInfo extends React.Component {

  static get propTypes() {
    return {
      site_name: PropTypes.string.isRequired,
    }
  }

  constructor(props) {
    super(props)
  }

  _create_info_class() {
    const IMG_INFO = SEARCH_SITE_INFO[this.props.site_name].img_info

    let class_name = 'img-info'
    if(IMG_INFO.user_registration) {
      class_name += ' active--user-registration'
    }
    if(IMG_INFO.credit) {
      class_name += ' active--credit'
    }
    if(IMG_INFO.not_free) {
      class_name += ' active--not-free'
    }
    return class_name
  }

  render() {
    return (
      React.createElement (
        'p', {className: this._create_info_class()},
        React.createElement (
          'i', {className: 'fas fa-yen-sign not-free'}
        ),
        React.createElement (
          'i', {className: 'far fa-copyright credit'}
        ),
        React.createElement (
          'i', {className: 'fas fa-user user-registration'}
        )
      )
    )
  }
}