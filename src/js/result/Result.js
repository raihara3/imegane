import React from 'react'
import PropTypes from 'prop-types'

import ImgInfo from './ImgInfo'
import BookmarkBtn from '../bookmark/BookmarkBtn'
import MagnifyImg from './MagnifyImg'

export default class Result extends React.Component {

  static get propTypes() {
    return {
      site_name: PropTypes.array.isRequired,
      img_src_list: PropTypes.array.isRequired,
      dl_url_list: PropTypes.array.isRequired
    }
  }

  constructor(props) {
    super(props)
  }

  _array_sorft() {
    let img_list = this.props.img_src_list
    let url_list = this.props.dl_url_list
    let site_name_list = this.props.site_name

    for(let index = 1; index < img_list.length - 1; index++){
      let replace_index = Math.floor(Math.random() * (index - 1))
      let img_tmp = img_list[index]
      let url_tmp = url_list[index]
      let site_name_tmp = site_name_list[index]

      img_list[index] = img_list[replace_index]
      url_list[index] = url_list[replace_index]
      site_name_list[index] = site_name_list[replace_index]

      img_list[replace_index] = img_tmp
      url_list[replace_index] = url_tmp
      site_name_list[replace_index] = site_name_tmp
    }
    return {
      'img_list': img_list,
      'url_list': url_list,
      'site_name_list': site_name_list
    }
  }

  render() {
    const result = this._array_sorft();
    let sorted_img_list = result.img_list
    let sorted_url_list = result.url_list
    let sorted_site_name_list = result.site_name_list

    const result_contents = sorted_img_list.map((_, i) => {
      return (
        React.createElement (
          'div', {className: 'result-box'},
          React.createElement (
            'div', {className: 'img-box'},
            React.createElement (
              'a', {href: sorted_url_list[i], target: '_blank'},
              React.createElement(
                'img', {src: sorted_img_list[i]}
              )
            )
          ),

          React.createElement (
            'div', {className: 'img-info-box'},
            React.createElement (
              'p', {className: 'site-name'}, sorted_site_name_list[i]
            ),
            React.createElement (
              ImgInfo, {site_name: sorted_site_name_list[i]}
            )
          ),

          React.createElement (
            BookmarkBtn, {download_url: sorted_url_list[i], img_src: sorted_img_list[i],  site_name: sorted_site_name_list[i]}
          ),

          React.createElement (
            MagnifyImg, {download_url: sorted_url_list[i], img_src: sorted_img_list[i]}
          )
        )
      )
    })

    return (
      result_contents
    )
  }
}