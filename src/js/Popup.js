import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

export default class Popup extends React.Component {

  static get propTypes () {
    return {
      text: PropTypes.string.isRequired
    }
  }

  constructor(props) {
    super(props)
  }

  _delete_popup() {
    setTimeout(() => {
      ReactDOM.unmountComponentAtNode (
        document.getElementById('popup')
      )
    }, 3000)
  }

  render() {
    this._delete_popup()
    return (
      React.createElement (
        'div', {className: 'popup-box'},
        React.createElement (
          'div', {className: 'popup-txt-box'}, this.props.text
        )
      )
    )
  }
}