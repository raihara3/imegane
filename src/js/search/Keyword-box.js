import React from 'react'

import Search from './Search'

export default class KeywordBox extends React.Component {

  constructor(props) {
    super(props)
  }

  _on_key_down(e) {
    sessionStorage.setItem('search_keyword', e.target.value)
    if(e.keyCode != 13) return
    const TEXT_BOX = document.activeElement
    TEXT_BOX.blur()
    console.log('enter')
    const SEARCH_INDEX = 1
    sessionStorage.setItem('search_index', SEARCH_INDEX)

    const SEARCH = new Search
    SEARCH._search_start(SEARCH_INDEX)
  }

  render() {
    return (
      React.createElement (
        'div', {className: 'search__keyword-box'},
        React.createElement (
          'i', {className: 'fas fa-search'}
        ),
        React.createElement (
          'input', {type: 'text', onChange: this._on_key_down, onKeyDown: this._on_key_down}
        ),
        React.createElement (
          'div', {className: 'search__select-box'},
          React.createElement (
            'select', null,
            React.createElement (
              'option', null, 'Photo'
            ),
            // React.createElement (
            //   'option', null, 'Illustration'
            // ),
            // React.createElement (
            //   'option', null, 'icon'
            // )
          )
        )
      )
    )
  }
}