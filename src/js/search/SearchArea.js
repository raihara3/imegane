import React from 'react'
import ReactDOM from 'react-dom'

import KeywordBox from './Keyword-box'
import Search from './Search'

class SearchArea extends React.Component {

  constructor(props) {
    super(props)
  }

  _search_start() {
    console.log('button click')
    const SEARCH_KEYWORD = sessionStorage.getItem('search_keyword')
    if(SEARCH_KEYWORD == null) return
    const TEXT_BOX = document.activeElement
    TEXT_BOX.blur()

    const SEARCH_INDEX = 1;
    sessionStorage.setItem('search_index', SEARCH_INDEX)

    const SEARCH = new Search
    SEARCH._search_start(SEARCH_INDEX)
  }

  render() {
    return (
      React.createElement (
        'div', null,
        React.createElement (
          KeywordBox
        ),
        React.createElement (
          'div', {className: 'search-btn', onClick: this._search_start}, 'SEARCH'
        )
      )
    )
  }
}

ReactDOM.render (
  React.createElement(SearchArea, null),
  document.getElementById('search-box')
)