import React from 'react'
import ReactDOM from 'react-dom'

import SearchSiteInfo from '../SearchSiteInfo'
import Scraping from './Scraping'
import Result from '../result/Result'
import PageNation from './PageNation'
import Loading from './Loading'

export default class Search {

  _search_start(SEARCH_INDEX) {
    console.log('search start ...')
    ReactDOM.render (
      React.createElement (
        Loading
      ),
      document.getElementById('loading-container')
    )

    const SEARCH_KEYWORD = sessionStorage.getItem('search_keyword')
    const SEARCH_KEYWORD_KEYS = Object.keys(SearchSiteInfo)

    let stock_rsite_name = []
    let stock_results__src = []
    let stock_results__href = []

    const SCRAPING = new Scraping

    const GET_CONTENTS = () => {
      let count = 0
      return new Promise((resolve) => {
        SEARCH_KEYWORD_KEYS.forEach((site_name) => {
          SCRAPING.get_contents_data(site_name, SEARCH_KEYWORD, SEARCH_INDEX, (data) => {  // data: [img src, download url]
            stock_rsite_name = stock_rsite_name.concat(data[0])
            stock_results__src = stock_results__src.concat(data[1])
            stock_results__href = stock_results__href.concat(data[2])

            count++
            if(count == SEARCH_KEYWORD_KEYS.length) {
              resolve()
            }
          })
        })
      })
    }
    GET_CONTENTS().then(
      () => {
        ReactDOM.unmountComponentAtNode (
          document.getElementById('loading-container')
        )
        this._render_result(stock_rsite_name, stock_results__src, stock_results__href)
      }
    )
  }

  _render_result(stock_rsite_name, stock_results__src, stock_results__href) {
    ReactDOM.render(
      React.createElement (
        'div', null,
        React.createElement(
          Result, {site_name: stock_rsite_name, img_src_list: stock_results__src, dl_url_list: stock_results__href}
        ),
        React.createElement (
          PageNation
        )
      ),
      document.getElementById('result-container')
    )
  }

}