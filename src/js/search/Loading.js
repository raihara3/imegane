import React from 'react'

export default class Loading extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      React.createElement (
        'div', {className: 'loading-box'},
        React.createElement (
          'div', {className: 'spinner'},
          React.createElement (
            'div', {className: 'cube1'}
          ),
          React.createElement (
            'div', {className: 'cube2'}
          )
        )
      )
    )
  }
}