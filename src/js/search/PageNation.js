import React from 'react'

import Search from './Search'

export default class PageNation extends React.Component {

  constructor(props) {
    super(props)
  }

  _on_click_back() {
    let SEARCH_INDEX = Number(sessionStorage.getItem('search_index'))
    SEARCH_INDEX--
    sessionStorage.setItem('search_index', SEARCH_INDEX)

    const SEARCH = new Search
    SEARCH._search_start(SEARCH_INDEX)
  }

  _on_click_next() {
    let SEARCH_INDEX = Number(sessionStorage.getItem('search_index'))
    SEARCH_INDEX++
    sessionStorage.setItem('search_index', SEARCH_INDEX)

    const SEARCH = new Search
    SEARCH._search_start(SEARCH_INDEX)
  }

  render() {
    window.scroll(0,320)

    const CREATE_CLASS_NAME = () => {
      let class_name = 'far fa-caret-square-left'
      if(Number(sessionStorage.getItem('search_index')) == 1) {
        class_name += ' gray-out'
      }
      return class_name
    }

    return (
      React.createElement (
        'ul', {className: 'page-nation'},
        React.createElement (
          'li', null,
          React.createElement (
            'i', {className: CREATE_CLASS_NAME(), onClick: this._on_click_back}
          )
        ),
        React.createElement (
          'li', {className: 'page-number'}, sessionStorage.getItem('search_index')
        ),
        React.createElement (
          'li', null,
          React.createElement (
            'i', {className: 'far fa-caret-square-right', onClick: this._on_click_next}
          )
        )
      )
    )
  }
}