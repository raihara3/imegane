const SEARCH_SITE_INFO = require('../SearchSiteInfo')

export default class Scraping {

  get_contents_data(SITE_NAME, KEYWORD, PAGE, CALLBACK) {
    const SITE_INFO = SEARCH_SITE_INFO[SITE_NAME]
    let PARAMETER = SITE_INFO.parameter
    PARAMETER = PARAMETER.replace(/{keyword}/g, KEYWORD)
    PARAMETER = PARAMETER.replace(/{page}/g, PAGE)
    const SEARCH_URL = encodeURI(SITE_INFO.service_top + SITE_INFO.search_directory + PARAMETER)

    fetch('http://proxy.imagemegane.com/?url=' + SEARCH_URL, {
    // fetch('./api/?url=' + SEARCH_URL, {
      method: 'GET',
      // body: TARGET_URL
    }).then(response => response.json())
    .then(HTML => {
      this._cutting_contents_data(SITE_NAME, HTML, CALLBACK)
    })
  }

  _cutting_contents_data(SITE_NAME, HTML, CALLBACK) {
    const PARSER = new DOMParser()
    const DOCMENT = PARSER.parseFromString(HTML, "text/html")
    const SITE_INFO = SEARCH_SITE_INFO[SITE_NAME]

    let target_contents
    let search_type
    if(SITE_INFO.get_contents['id_or_class'] == 'class') {
      search_type = 'class'
      target_contents = DOCMENT.getElementsByClassName(SITE_INFO.get_contents['get_name'])
    }else if(SITE_INFO.get_contents['id_or_class'] == 'id') {
      search_type = 'id'
      target_contents = DOCMENT.getElementById(SITE_INFO.get_contents['get_name'])
    }

    this._cutting_url(SITE_NAME, SITE_INFO, search_type, target_contents, CALLBACK)
  }

  _cutting_url(SITE_NAME, SITE_INFO, SEARCH_TYPE, TARGET_CONTENTS, CALLBACK) {
    let remove_img_index = []

    let site_name = []
    let src_list = []
    let img_src

    for(let i = 0; i < TARGET_CONTENTS.length; i++) {
      let get_src_attribute = SITE_INFO.get_src_attribute
      if(get_src_attribute == 'src') {
        img_src = TARGET_CONTENTS[i].getElementsByTagName('img')[0].src
      }else if(get_src_attribute == 'style') {
        img_src = TARGET_CONTENTS[i].style.backgroundImage
        img_src = img_src.slice(img_src.indexOf('("') + 2, img_src.lastIndexOf('")'))
      }else {
        img_src = TARGET_CONTENTS[i].getElementsByTagName('img')[0].dataset[get_src_attribute]
      }

      if(img_src == null || (SITE_NAME != 'photo-ac' && img_src.indexOf(SITE_INFO.service_name) == -1)) {
        remove_img_index.push(i)
      }else {
        src_list.push(img_src)
        site_name.push(SITE_NAME)
      }
    }

    let href_list = []
    let a_href
    let get_href_attribute = SITE_INFO.get_href_attribute
    for(let i = 0; i < TARGET_CONTENTS.length; i++) {
      if(remove_img_index[0] != i) {
        if(SITE_INFO.get_href_attribute == 'href') {
          a_href = TARGET_CONTENTS[i].getElementsByTagName('a')[0].href
        }else {
          a_href = TARGET_CONTENTS[i].getElementsByTagName('a')[0].dataset[get_href_attribute]
        }
        a_href = a_href.replace(window.location.href, '')
        href_list.push(SITE_INFO.download_directory + a_href)
      }else {
        remove_img_index.shift()
      }
    }
    CALLBACK([site_name, src_list, href_list])
  }
}