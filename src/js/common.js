import React from 'react'


import './bookmark/Bookmark'
import './search/SearchArea'
import './SearchSiteList'

import MagnifyBox from './result/MagnifyBox'

document.onkeydown = (e) => {
  let magnify_box = document.getElementById('magnify-box')
  if(e.keyCode != 27 || magnify_box.innerHTML == '') return

  const MAGNIFYBOX = new MagnifyBox()
  MAGNIFYBOX._close_box()
}