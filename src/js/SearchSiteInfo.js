// template
// ------
// {service_key}: {
//   service_name: {name},
//   service_logo: {url},
//   service_top: {url},
//   search_directory: {url},
//   download_directory: {url},
//   parameter: {parameter},
//   get_contents: {
//     id_or_class: {name},
//     get_name: {name}
//   },
//   get_src_attribute: {attribute},
//   get_href_attribute: {attribute},
//   ser_registration: {bool}
// }
// ------


// {keyword} : Enter search keywords
// {page} : Enter search page number
module.exports = {

  'photo-ac': {
    service_name: 'photo AC',
    service_logo: 'https://jp.static.photo-ac.com/assets/img/photoAC_new.png',
    service_top: 'https://www.photo-ac.com',
    search_directory: '/main/search',
    download_directory: 'https://www.photo-ac.com/main/detail/',
    parameter: '?q={keyword}&qt=&qid=&creator=&ngcreator=&nq=&srt=dlrank&orientation=all&sizesec=all&mdlrlrsec=all&sl=ja&pp=70&p={page}',
    get_contents: {
      id_or_class: 'class',
      get_name: 'photo-img'
    },
    get_src_attribute: 'srcorg',
    get_href_attribute: 'id',
    img_info: {
      user_registration: true,
      credit: false,
      not_free: false
    }
  },

  'pixabay': {
    service_name: 'pixabay',
    service_logo: 'http://www.evelynmcmarketing.com/wp-content/uploads/2017/04/Pixabay-Logo.png',
    service_top: 'https://pixabay.com',
    search_directory: '/ja/photos',
    download_directory: 'https://pixabay.com/',
    parameter: '?q={keyword}&image_type=all&pagi={page}',
    get_contents: {
      id_or_class: 'class',
      get_name: 'item'
    },
    get_src_attribute: 'lazy',
    get_href_attribute: 'href',
    img_info: {
      user_registration: false,
      credit: false,
      not_free: false
    }
  },

  'freephotos.cc': {
    service_name: 'freephotos.cc',
    service_logo: 'https://freephotos.cc/assets/img/main.jpg',
    service_top: 'https://freephotos.cc/ja',
    search_directory: '/',
    download_directory: '',
    parameter: '{keyword}',
    get_contents: {
      id_or_class: 'class',
      get_name: 'grid-item'
    },
    get_src_attribute: 'style',
    get_href_attribute: 'download',
    img_info: {
      user_registration: false,
      credit: false,
      not_free: false
    }
  },

  'foodiesfeed': {
    service_name: 'foodiesfeed',
    service_logo: 'https://www.foodiesfeed.com/wp-content/themes/foodiesfeed/library/images/foodiesfeed.svg',
    service_top: 'https://www.foodiesfeed.com',
    search_directory: '/',
    download_directory: '',
    parameter: '?s={keyword}',
    get_contents: {
      id_or_class: 'class',
      get_name: 'post-item'
    },
    get_src_attribute: 'src',
    get_href_attribute: 'href',
    img_info: {
      user_registration: false,
      credit: false,
      not_free: false
    }
  },

  'freerangestock': {
    service_name: 'freerangestock',
    service_logo: 'https://freerangestock.com/templates/v5/images/FRS-FREERANGE.png',
    service_top: 'https://freerangestock.com',
    search_directory: '/search.php',
    download_directory: '',
    parameter: '?search={keyword}&type=photo&page_num={page}&perpage=50',
    get_contents: {
      id_or_class: 'class',
      get_name: 'item'
    },
    get_src_attribute: 'src',
    get_href_attribute: 'href',
    img_info: {
      user_registration: true,
      credit: false,
      not_free: false
    }
  },

  'visualhunt': {
    service_name: 'visualhunt',
    service_logo: 'https://visualhunt.com/fo/img/logo-v1-large.png',
    service_top: 'https://visualhunt.com',
    search_directory: '/search/instant/',
    download_directory: 'https://visualhunt.com/',
    parameter: '?q={keyword}',
    get_contents: {
      id_or_class: 'class',
      get_name: 'vh-Collage-item'
    },
    get_src_attribute: 'src',
    get_href_attribute: 'href',
    img_info: {
      user_registration: false,
      credit: true,
      not_free: false
    }
  },

  'shotstash': {
    service_name: 'shotstash',
    service_logo: 'https://shotstash.com/wp-content/themes/shotstash-v2.0/images/logo.png',
    service_top: 'https://shotstash.com',
    search_directory: '/page/',
    download_directory: '',
    parameter: '{page}/?s={keyword}',
    get_contents: {
      id_or_class: 'class',
      get_name: 'post'
    },
    get_src_attribute: 'src',
    get_href_attribute: 'href',
    img_info: {
      user_registration: false,
      credit: false,
      not_free: false
    }
  },

  'gratisography': {
    service_name: 'gratisography',
    service_logo: 'https://gratisography.com/wp-content/themes/gratis-v2.2/images/favicon.ico',
    service_top: 'https://gratisography.com',
    search_directory: '/page/',
    download_directory: '',
    parameter: '{page}/?s={keyword}',
    get_contents: {
      id_or_class: 'class',
      get_name: 'single-photo'
    },
    get_src_attribute: 'src',
    get_href_attribute: 'href',
    img_info: {
      user_registration: false,
      credit: false,
      not_free: false
    }
  },

  'unsplash': {
    service_name: 'unsplash',
    service_logo: 'https://source.unsplash.com/assets/open-graph-e9fd6b8b42e121e1ca214d94f0551408baad7c04a1c3b63b230e6d62b6f1e28e.jpg',
    service_top: 'https://unsplash.com',
    search_directory: '/search/photos/',
    download_directory: 'https://unsplash.com/',
    parameter: '{keyword}',
    get_contents: {
      id_or_class: 'class',
      get_name: '_1pn7R'
    },
    get_src_attribute: 'src',
    get_href_attribute: 'href',
    img_info: {
      user_registration: false,
      credit: false,
      not_free: false
    }
  }

}