import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

import Bookmark from './Bookmark'
import Popup from '../Popup'

export default class BookmarkBtn extends React.Component {

  static get propTypes () {
    return {
      download_url: PropTypes.string.isRequired,
      img_src: PropTypes.string.isRequired,
      site_name: PropTypes.string.isRequired
    }
  }

  constructor(props) {
    super(props)
    this._onClick = this._onClick.bind(this);
  }

  _onClick() {
    const BOOKMARK = new Bookmark
    BOOKMARK._add_bookmark(this.props.download_url, this.props.img_src, this.props.site_name)

    // ReactDOM.render (
    //   React.createElement (
    //     Popup, {text: 'ブックマークに追加しました。'}
    //   ),
    //   document.getElementById('popup')
    // )
  }

  render() {
    return (
      React.createElement (
        'div', {className: 'bookmark-btn', onClick: this._onClick},
        React.createElement (
          'i', {className: 'fas fa-book'}
        )
      )
    )
  }
}