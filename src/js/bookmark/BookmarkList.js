import React from 'react'
import PropTypes from 'prop-types'

import BookmarkImg from './BookmarkImg'

export default class BookmarkList extends React.Component {

  static get propTypes() {
    return {
      url_list: PropTypes.array.isRequired,
      src_list: PropTypes.array.isRequired,
      site_name: PropTypes.array.isRequired,
      list_open: PropTypes.bool.isRequired
    }
  }

  constructor(props) {
    super(props)
  }

  render() {
    const BOOKMARK_LIST = this.props.url_list.map((_, index) => {
      return (
        React.createElement (
          BookmarkImg, {url_list: this.props.url_list[index], src_list: this.props.src_list[index], site_name: this.props.site_name[index]}
        )
      )
    })

    const LIST_CLASS_NAME = () => {
      let class_name = 'bookmark-list-container'
      if(this.props.list_open) {
        class_name += ' open'
      }
      return class_name
    }

    return (
      React.createElement (
        'div', {className: LIST_CLASS_NAME()},
        React.createElement (
          'ul', {className: 'bookmark-list cf'}, BOOKMARK_LIST
        )
      )
    )
  }
}