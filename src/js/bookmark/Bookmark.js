import React from 'react'
import ReactDOM from 'react-dom'

import BookmarkList from './BookmarkList'

export default class Bookmark extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      url_list: [],
      src_list: [],
      site_name: [],
      list_open: false
    }
    this._list_open_or_close = this._list_open_or_close.bind(this)
  }

  _get_bookmark() {
    const BOOKMARK_URL = sessionStorage.getItem('bookmark_url')
    if(BOOKMARK_URL == '' || BOOKMARK_URL == null) {
      this.state.url_list = []
      this.state.src_list = []
      this.state.site_name = []
      return
    }
    this.state.url_list = BOOKMARK_URL.split(',')
    const BOOKMARK_SRC = sessionStorage.getItem('bookmark_src')
    const BOOKMARK_SITE_NAME = sessionStorage.getItem('bookmark_site_name')
    this.state.src_list = BOOKMARK_SRC.split(',')
    this.state.site_name = BOOKMARK_SITE_NAME.split(',')
  }

  _count_up() {
    const COUNT = Number(sessionStorage.getItem('bookmark_cnt')) + 1
    sessionStorage.setItem('bookmark_cnt', COUNT)
  }

  _count_down() {
    if(Number(sessionStorage.getItem('bookmark_cnt')) == 0) {
      console.error('It can not be deleted any more.')
      return
    }
    const COUNT = Number(sessionStorage.getItem('bookmark_cnt')) - 1
    sessionStorage.setItem('bookmark_cnt', COUNT)
  }

  _add_bookmark(DOWNLOAD_URL, IMG_SRC, SITE_NAME) {
    const EXISTING_BOOKMARK_URL = sessionStorage.getItem('bookmark_url')
    const EXISTING_BOOKMARK_SRC = sessionStorage.getItem('bookmark_src')
    const EXISTING_BOOKMARK_SITE_NAME = sessionStorage.getItem('bookmark_site_name')
    if(EXISTING_BOOKMARK_URL == null) {
      sessionStorage.setItem('bookmark_url', DOWNLOAD_URL)
      sessionStorage.setItem('bookmark_src', IMG_SRC)
      sessionStorage.setItem('bookmark_site_name', SITE_NAME)
    }else {
      var existing_url = String(EXISTING_BOOKMARK_URL)
      if(existing_url.match(DOWNLOAD_URL) != null) return
      sessionStorage.setItem('bookmark_url', EXISTING_BOOKMARK_URL + ',' + DOWNLOAD_URL)
      sessionStorage.setItem('bookmark_src', EXISTING_BOOKMARK_SRC + ',' + IMG_SRC)
      sessionStorage.setItem('bookmark_site_name', EXISTING_BOOKMARK_SITE_NAME + ',' + SITE_NAME)
    }
    this._count_up()
    RENDER_BOOKMARK()
  }

  _delete_bookmark(DOWNLOAD_URL) {
    const STRAGE_URL_LIST = sessionStorage.getItem('bookmark_url').split(',')
    const STRAGE_SRC_LIST = sessionStorage.getItem('bookmark_src').split(',')
    const STRAGE_SITE_NAME_LIST = sessionStorage.getItem('bookmark_site_name').split(',')
    const INDEX = STRAGE_URL_LIST.indexOf(DOWNLOAD_URL)
    STRAGE_URL_LIST.splice(INDEX, 1)
    STRAGE_SRC_LIST.splice(INDEX, 1)
    if(STRAGE_URL_LIST.length == 0) {
      sessionStorage.removeItem('bookmark_url')
      sessionStorage.removeItem('bookmark_src')
      sessionStorage.removeItem('bookmark_site_name')
    }else {
      STRAGE_SITE_NAME_LIST.splice(INDEX, 1)
      sessionStorage.setItem('bookmark_url', STRAGE_URL_LIST)
      sessionStorage.setItem('bookmark_src', STRAGE_SRC_LIST)
      sessionStorage.setItem('bookmark_site_name', STRAGE_SITE_NAME_LIST)
    }
    this._count_down()
    RENDER_BOOKMARK()
  }

  _list_open_or_close() {
    this.setState({list_open: !this.state.list_open})
  }

  render() {
    this._get_bookmark()
    return (
      React.createElement (
        'div', null,
        React.createElement (
          'div', {className: 'bookmark-area_btn', onClick: this._list_open_or_close},
          React.createElement (
            'i', {className: 'fas fa-book'}
          ),
          React.createElement (
            'div', {className: 'bookmark-cnt'}, sessionStorage.getItem('bookmark_cnt')
          )
        ),

        React.createElement (
          BookmarkList, {url_list: this.state.url_list, src_list: this.state.src_list, site_name: this.state.site_name, list_open: this.state.list_open}
        )
      )
    )
  }
}

const RENDER_BOOKMARK = () => {
  ReactDOM.render (
    React.createElement (Bookmark, null),
    document.getElementById('bookmark-area')
  )
}

(() => {
  if(sessionStorage.getItem('bookmark_cnt') == null) {
    sessionStorage.setItem('bookmark_cnt', 0)
  }
  RENDER_BOOKMARK()
})()