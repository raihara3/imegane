import React from 'react'
import PropTypes from 'prop-types'

import Bookmark from './Bookmark'
import ImgInfo from '../result/ImgInfo'

export default class BookmarkImg extends React.Component {

  static get propTypes() {
    return {
      url_list: PropTypes.string.isRequired,
      src_list: PropTypes.string.isRequired,
      site_name: PropTypes.string.isRequireds
    }
  }

  constructor(props) {
    super(props)
    this._delete_bookmark = this._delete_bookmark.bind(this)
  }

  _delete_bookmark() {
    const DOWNLOAD_URL = this.props.url_list
    const BOOKMARK = new Bookmark
    BOOKMARK._delete_bookmark(DOWNLOAD_URL)
  }

  render() {
    return (
      React.createElement (
        'li', null,
        React.createElement (
          'a', {href: this.props.url_list, target: '_blank'},
          React.createElement (
            'img', {src: this.props.src_list}
          )
        ),

        React.createElement (
          'div', {className: 'delete-bookmark-btn', onClick: this._delete_bookmark},
          React.createElement (
            'i', {className: 'fas fa-times'}
          )
        ),

        React.createElement (
          'div', {className: 'img-info-box'},
          React.createElement (
            'p', {className: 'site-name'}, this.props.site_name
          ),
          React.createElement (
            ImgInfo, {site_name: this.props.site_name}
          )
        ),
      )
    )
  }
}