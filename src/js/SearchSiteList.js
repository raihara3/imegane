import React from 'react'
import ReactDOM from 'react-dom'

const SEARCH_SITE_INFO = require('./SearchSiteInfo')

export default class SearchSiteList extends React.Component {

  constructor(props) {
    super(props)
  }

  _search_site_cnt() {
    const keys = Object.keys(SEARCH_SITE_INFO)
    return keys.length
  }

  render() {
    const keys = Object.keys(SEARCH_SITE_INFO)
    const site_logos = keys.map((_, index) => {
      const TARGET_SITE_INFO = SEARCH_SITE_INFO[keys[index]]
      return (
        React.createElement (
          'li', null,
          React.createElement (
            'a', {href: TARGET_SITE_INFO.service_top, target: '_blank'},
            React.createElement (
              'img', {src: TARGET_SITE_INFO.service_logo}
            )
          )
        )
      )
    })

    return (
      React.createElement (
        'div', null,
        React.createElement (
          'h2', {className: 'site-description'}, '複数の素材配布サービスから、最高の1枚を見つける。'
        ),
        React.createElement (
          'p', {className: 'site-cnt'}, '現在',
          React.createElement (
            'span', {className: 'site-cnt--point'}, this._search_site_cnt()
          ),
          React.createElement (
            'span', null, 'サイト'
          ),
          'から検索可能'
        ),
        React.createElement (
          'ul', {className: 'site-list'}, site_logos
        )
      )
    )
  }
}

ReactDOM.render (
  React.createElement (SearchSiteList, null),
  document.getElementById('search-site-list')
)