const path = require('path');
const webpackMerge = require('webpack-merge');

const baseConfig = {
  mode: 'development',
  watch: true,
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query:{
          presets: ['@babel/preset-react', '@babel/preset-env']
        }
      }
    ],
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    port: 3000,
  }
}

const frontConfig = webpackMerge(baseConfig, {
  target: 'web',
  entry: './src/js/common.js',
  output: {filename: './js/bundle.js'},
})

const nodeConfig = webpackMerge(baseConfig, {
  target: 'node',
  entry: './src/api/scraping.js',
  output: {filename: './api/scraping.js'},
})

module.exports = [frontConfig, nodeConfig]